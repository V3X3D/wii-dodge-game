# Building this bad boy

---

## Install Devkitpro
---
Install devkitpro, this is well documented so read the wiki

https://devkitpro.org/wiki/devkitPro_pacman

These are the packages you should install not all are required but grab the lot.
Things might work with other versions as well, but the version numbers are
listed just incase.

dkp-pacman -S wii-dev \
ppc-bzip2 \
ppc-freetype \
ppc-glm \
ppc-libgd \
ppc-libjpeg-turbo \
ppc-libogg \
ppc-libpng \
ppc-libvorbisidec \
ppc-mpg123 \
ppc-mxml \
ppc-pkg-config \
ppc-zlib

## Install GRRLIB
---
The version of GRRLIB used.
https://github.com/GRRLIB/GRRLIB/releases/tag/4.3.2

Download and extract.

cd into the GRRLIB dir, ignore the examples for now, then make the files
sudo make

Each lib subfolder will have the respctive file (listed below)

libgrrlib.a
libjpeg.a
libpng.a
libpngu.a
libz.a
libfreetype.a

Copy these to
the lib folder at the root of Wii-Dodge-Game/lib

Also inside GRRLIB copy everything in the GRRLIB folder

These go to the Wii-Dodge-Game/include folder

## Make
---
In the root of Wii-Dodge-Game run 'make' or 'sudo make'
Then test with dolphin or HBC


