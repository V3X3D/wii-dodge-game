# Acid Rain - A Wii game about dodging squares

## How do I play?

Follow the `How to build` instructions below.

Once that is done, just start up the `boot.dol` file in the Home Brew Channel (HBC), or on an emulator.
- Movement: Point the wiimote at the screen and dodge away!
- Pause:    Home Button.

## How to build

Check the `BUILD.md` for full details on how to build Acid Rain.

## Project Is No More

I'm no longer going to be working on this, it is technically a full game.

The code is quite awful as well, lots of things I would do differently. (It has now been cleaned a little, but still awful)

## Notes

This project was never going to be a well polished game, full of dazzling effects, to keep the player gawking. It was just a small game I made to learn C.
