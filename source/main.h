#ifndef Main_File
#define Main_File

#include <grrlib.h>

#include <stdlib.h>
#include <string.h>
#include <wiiuse/wpad.h>
#include <asndlib.h>
#include <mp3player.h>

#include "dev/Fps.h"
#include "State.h"
#include "General.h"
#include "Controller.h"
#include "Player.h"
#include "Acid.h"
#include "Button.h"

#endif
