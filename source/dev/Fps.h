#ifndef Fps_Files
#define Fps_Files

#include <grrlib.h>

#include <ogc/lwp_watchdog.h>   // Needed for gettime and ticks_to_millisecs
#include <time.h>

u8 FPS;
u8 CalculateFrameRate();

#endif
