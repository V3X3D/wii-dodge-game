#include "State.h"

static void State_Quit();

void State_Init() {
  State.Home = State_Home;
  State.Join = State_Join;
  State.Play = State_Play;
  State.Quit = State_Quit;
}

static void State_Quit() {
  int i;
  //Free object mallocs
  for(i=0; i<MAX_ACID; i++)
    Acid.Delete(i);
  for(i=0; i<MAX_PLAYER; i++)
    Player.Delete(i);
  for(i=0; i<MAX_CONTROLLER; i++)
    Controller.Delete(i);

  //Free some textures
  GRRLIB_FreeTexture(font2.tex);
  GRRLIB_FreeTexture(font5.tex);
  GRRLIB_Exit();

  exit(0);
}
