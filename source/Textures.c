#include "Textures.h"

void Textures_Init() {
  font2.tex = GRRLIB_LoadTexture(BMfont2);
  font2.w = 16;
  font2.h = 16;
  GRRLIB_InitTileSet(font2.tex, font2.w, font2.h, 32);

  font5.tex = GRRLIB_LoadTexture(BMfont5);
  font5.w = 8;
  font5.h = 13;
  GRRLIB_InitTileSet(font5.tex, font5.w, font5.h, 0);
}
