#include "Join.h"

static int i;
static bool init_buttons = false;
static Button_Obj btn1;
static Button_Obj btn2;

//Join Screen (Menu before match starts) ---------------------------------------
void State_Join() {

  if(!init_buttons) {
    btn1 = Button.Make(SCREEN_WIDTH/2-Btn_Generic.w/2-Btn_Generic.w, 350, Btn_Generic, Button_Global_State_Change, HOME, "HOME");
    btn2 = Button.Make(SCREEN_WIDTH/2-Btn_Generic.w/2+Btn_Generic.w, 350, Btn_Generic, Button_Global_State_Change, PLAY, "START");
    init_buttons = true;
  }

  //Don't show acid on this screen, also clears them before game starts
  for(int i=0; i<MAX_ACID; i++)
    Acid.Delete(i);

  GRRLIB_Printf(60, 70, font2.tex, GRRLIB_WHITE, 0.75, "(PLUS) JOIN");
  GRRLIB_Printf(400, 70, font2.tex, GRRLIB_WHITE, 0.75, "(MINUS) CANCLE");

  for(i=0; i<MAX_PLAYER; i++) {
    if(Player.array[i] != NULL) {
      GRRLIB_Printf(54+(i*140), 120, font2.tex, Player.array[i]->color, 0.75, "P%d JOINED", i+1);
      if(Controller.array[i]->wpaddown & WPAD_BUTTON_MINUS) Player.Delete(i);
    } else {
      GRRLIB_Printf(54+(i*140), 120, font2.tex, GRRLIB_WHITE, 0.75, "P%d ......", i+1);
      if(Controller.array[i]->wpaddown & WPAD_BUTTON_PLUS) Player.Create(i);
    }
  }

  GRRLIB_Rectangle(0, SCREEN_HEIGHT/1.5, SCREEN_WIDTH, 100, 0xCCCCFF40, 1);
  Button.Action(&btn1);
  Button.Render(&btn1);
  Button.Action(&btn2);
  Button.Render(&btn2);

  for(i=0; i<MAX_CONTROLLER; i++)
    if(Controller.array[i] != NULL)
      Controller.Render(i);
}

