#include "Home.h"

static int i;
static bool init_buttons = false;
static Button_Obj btn1;
static Button_Obj btn2;
static Button_Obj btn2;

//Home Menu Screen -------------------------------------------------------------
void State_Home() {

  if(!init_buttons) {
    btn1 = Button.Make(SCREEN_WIDTH/2-Btn_Generic.w/2+Btn_Generic.w, 350, Btn_Generic, Button_Global_State_Change, JOIN, "PLAY");
    btn2 = Button.Make(SCREEN_WIDTH/2-Btn_Generic.w/2-Btn_Generic.w, 350, Btn_Generic, Button_Global_State_Change, QUIT, "QUIT");
    init_buttons = true;
  }

  //Making acid just to add a visual effect to the home screen
  Acid_Create_Rand_Timer(1, 50);
  for(i=0; i<MAX_ACID; i++) {
    if(Acid.array[i] != NULL) {
      Acid.Action(i);
      Acid.Render(i);
    }
  }

  GRRLIB_Rectangle(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT, 0x00000099, 1);
  GRRLIB_Printf(106, 62, font2.tex, GRRLIB_WHITE, 3, "ACID RAIN");

  GRRLIB_Rectangle(0, SCREEN_HEIGHT/1.5, SCREEN_WIDTH, 100, 0xCCCCFF40, 1);
  Button.Action(&btn1);
  Button.Render(&btn1);
  Button.Action(&btn2);
  Button.Render(&btn2);

  for(i=0; i<MAX_CONTROLLER; i++)
    if(Controller.array[i] != NULL)
      Controller.Render(i);
}

