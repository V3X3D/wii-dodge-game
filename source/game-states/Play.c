#include "Play.h"

static void Button_Play_State_Change(int state);
static void Reset_Vars();

static enum {
  GAME_RUNNING,
  GAME_STATS,
  GAME_RESET,
  GAME_EXIT
} playState;

static bool init_static_things = false;
static bool updated_stats_once = false;
static Button_Obj btn1;
static Button_Obj btn2;
static int i,
  chance,
  range = 200,
  decrease_range = 250,
  Stat_Menu_x = SCREEN_WIDTH/2-SCREEN_WIDTH/4,
  Stat_Menu_y = SCREEN_HEIGHT/2-SCREEN_HEIGHT/4;

//Game Playing -----------------------------------------------------------------
void State_Play() {

  if(!init_static_things) {
    for(i=0; i<MAX_PLAYER; i++)
      if(Player.array[i] != NULL) {
        Controller.array[i]->x = SCREEN_WIDTH/2-Player.array[i]->w/2;
        Controller.array[i]->y = SCREEN_HEIGHT/2-Player.array[i]->h/2;
      }

    btn1 = Button.Make(
      (SCREEN_WIDTH/2-Btn_Generic.w/2-Btn_Generic.w/2)-10,
      370,
      Btn_Generic,
      Button_Play_State_Change,
      GAME_RESET,
      "RESET"
    );
    btn2 = Button.Make(
      (SCREEN_WIDTH/2-Btn_Generic.w/2+Btn_Generic.w/2)+10,
      370,
      Btn_Generic,
      Button_Play_State_Change,
      GAME_EXIT,
      "HOME"
    );
    playState = GAME_RUNNING;
    init_static_things = true;
  }

  //Keeps the pace by ajusting chance
  if(Acid.count <= 4) chance = 9;
  else if(Acid.count <= 2) chance = 7;
  else chance = 5;
  //Always create acid even outside of GAME_RUNNING
  Acid_Create_Rand_Timer(chance, range);
  //Acid loop
  for(i=0; i<MAX_ACID; i++) {
    if(Acid.array[i] != NULL) {
      Acid.Action(i);
      Acid.Render(i);
    }
  }

  if(playState == GAME_RUNNING) {
    u8 Living_Player_Count = 4;

    if(globalScore == decrease_range) {
      //Every 250 points increase the chance for acid spawns
      range -= 20;
      decrease_range += 250;
    }

    for(i=0; i<MAX_PLAYER; i++)
      if(Player.array[i] != NULL) {
        GRRLIB_Printf(50+(i*144), 42, font2.tex, Player.array[i]->color, 1, "P%d: %d", i+1, Player.array[i]->score);
        Player.Action(i);
        Player.Render(i);
        if(Player.array[i]->dead) Living_Player_Count--;
      }
      else Living_Player_Count--; //Remove NULL players from count

    //Game Over
    if(Living_Player_Count == 0) playState = GAME_STATS;
  }

  if(playState == GAME_STATS) {
    //The Stats Backdrop
    GRRLIB_Rectangle(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT, 0x00000099, 1);
    GRRLIB_Rectangle( Stat_Menu_x, Stat_Menu_y, SCREEN_WIDTH/2, SCREEN_HEIGHT/2, 0xFFFFFFDD, 1);

    GRRLIB_Printf(Stat_Menu_x+3, Stat_Menu_y+5, font5.tex, GRRLIB_BLACK, 1, "Top Scores!");
    GRRLIB_Printf(Stat_Menu_x+3, Stat_Menu_y+15, font5.tex, GRRLIB_BLACK, 1, "---------------------------------------");

    //Ugly blob that updates stats screen with new high scores
    //Don't plan to clean this blob up, read README.md notes
    if(!updated_stats_once) {
      for(i=0; i<MAX_PLAYER; i++) {
        for(int z=0; z<10; z++) {
          if(Player.array[i] != NULL && Player.array[i]->score > globalHighScores[z]) {
            for(int k=10; k>z; k--) globalHighScores[k] = globalHighScores[k-1];
            globalHighScores[z] = Player.array[i]->score;
            z = 10;
          }
        }
      }
      updated_stats_once = true;
    }

    for(i=0; i<10; i++) {
      GRRLIB_Printf(
        Stat_Menu_x+15,
        (Stat_Menu_y+35)+(20*i),
        font5.tex,
        GRRLIB_BLACK,
        1,
        "%02d: %d",
        i+1,
        globalHighScores[i]
      );
    }

    Button.Action(&btn1);
    Button.Render(&btn1);
    Button.Action(&btn2);
    Button.Render(&btn2);

    for(i=0; i<MAX_CONTROLLER; i++)
      if(Controller.array[i] != NULL)
        Controller.Render(i);
  }

  if(playState == GAME_RESET) {
    Reset_Vars();
    for(i=0; i<MAX_PLAYER; i++) {
      if(Player.array[i] != NULL) {
        Player.array[i]->score = 0;
        Player.array[i]->dead = false;
      }
    }
    playState = GAME_RUNNING;
    globalState = PLAY;
  }

  if(playState == GAME_EXIT) {
    Reset_Vars();
    for(i=0; i<MAX_PLAYER; i++)
      Player.Delete(i);

    globalState = HOME;
  }
}

//OTHER FUNCTIONS ------------------
static void Reset_Vars() {
  //Cleanup Everything
  for(i=0; i<MAX_ACID; i++)
    Acid.Delete(i);

  //Reset Variables
  range = 200;
  decrease_range = 250;
  globalScore = 0;
  updated_stats_once = false;
  init_static_things = false;
}

static void Button_Play_State_Change(int state) { playState = state; }
