#ifndef Controller_File
#define Controller_File

#include <grrlib.h>

#include <stdlib.h>
#include <wiiuse/wpad.h>

#include "General.h"
#include "Player.h"

typedef struct {
  int self;
  int x;
  int y;
  int w;
  int h;
  int color;
  u32 wpaddown, wpadheld, wpadup;
  ir_t ir;
} Controller_Obj;

typedef struct {
  void (*Create)(int i);
  void (*Action)(int i);
  void (*Delete)(int i);
  void (*Render)(int i);
  Controller_Obj *array[MAX_CONTROLLER];
  int count;
} Controller_Class;
Controller_Class Controller;

void Controller_Init();

#endif
