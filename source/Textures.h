#ifndef Textures_File
#define Textures_File

#include <grrlib.h>

#include "gfx/BMfont2.h"
#include "gfx/BMfont5.h"

typedef struct {
  GRRLIB_texImg *tex;
  int w;
  int h;
} Font_Obj;

Font_Obj font2;
Font_Obj font5;

void Textures_Init();

#endif
