#include "main.h"

int main() {
  globalState = HOME;
  globalScore = 0;

  //Inits
  srand(time(NULL));
  //Graphics
  GRRLIB_Init();
  //Sound
	ASND_Init();
	MP3Player_Init();
  //My stuff
  Textures_Init();
  Controller_Init();
  Player_Init();
  Acid_Init();
  Button_Init();
  State_Init();

  //Ready controllers
  for(int i=0; i<MAX_CONTROLLER; i++)
    Controller.Create(i);

  while(1) {

    //Always update controller input
    WPAD_ScanPads();
    for(int i=0; i<MAX_CONTROLLER; i++)
      if(Controller.array[i] != NULL)
        Controller.Action(i);

    switch(globalState) {
      case INTRO:
        break;
      case HOME:
        State.Home();
        break;
      case JOIN:
        State.Join();
        break;
      case PLAY:
        State.Play();
        break;
      case EXIT:
        break;
      case QUIT:
        State.Quit();
        break;
    }

    /* GRRLIB_Printf(10, 20, font5.tex, GRRLIB_WHITE, 1, "FPS: %d", FPS); */
    FPS = CalculateFrameRate();

    GRRLIB_Render();
  }

  //Free object mallocs
  for(int i=0; i<MAX_ACID; i++)
    Acid.Delete(i);
  for(int i=0; i<MAX_PLAYER; i++)
    Player.Delete(i);
  for(int i=0; i<MAX_CONTROLLER; i++)
    Controller.Delete(i);

  //Free some textures
  GRRLIB_FreeTexture(font2.tex);
  GRRLIB_FreeTexture(font5.tex);
  GRRLIB_Exit();
  return 0;
}
