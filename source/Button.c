#include "Button.h"

static Button_Obj Button_Make(int x, int y, Button_Obj btn, void (*on_click)(int), int click_arg, char *text);
static void Button_Visual(Font_Obj font, int BGcolor, int BGcolor_hover, int FGcolor, int FGcolor_hover);
static void Button_Action(Button_Obj *btn);
static void Button_Render(Button_Obj *btn);

//Button OnClick Actions
void Button_Global_State_Change(int state) { globalState = state; }

//Button Visual Settings
typedef struct {
  int BGcolor_normal;
  int BGcolor_hover;
  int FGcolor_normal;
  int FGcolor_hover;
  Font_Obj font;
} Button_inherit;
static Button_inherit visual_inherit;

void Button_Init() {
  Button.Make   = Button_Make;
  Button.Visual = Button_Visual;
  Button.Action = Button_Action;
  Button.Render = Button_Render;

  visual_inherit.font           = font2;  
  visual_inherit.BGcolor_normal = GRRLIB_GREEN;
  visual_inherit.FGcolor_normal = GRRLIB_WHITE;
  visual_inherit.BGcolor_hover  = GRRLIB_DARKGREEN;
  visual_inherit.FGcolor_hover  = GRRLIB_WHITE;

  //Set a generic button
  Btn_Generic.w = 150;
  Btn_Generic.h = 40;
}

Button_Obj Button_Make(int x, int y, Button_Obj btn, void (*on_click)(int), int click_arg, char *text) {
  btn.x = x;
  btn.y = y;
  btn.on_click = on_click;
  btn.click_arg = click_arg;
  btn.text = text;

  btn.font           = visual_inherit.font;
  btn.BGcolor_normal = visual_inherit.BGcolor_normal;
  btn.FGcolor_normal = visual_inherit.FGcolor_normal;
  btn.BGcolor_hover  = visual_inherit.BGcolor_hover;
  btn.FGcolor_hover  = visual_inherit.FGcolor_hover;
  btn.BGcolor        = visual_inherit.BGcolor_normal;
  btn.FGcolor        = visual_inherit.FGcolor_normal;

  return btn;
}

//Assigns visuals to following Button.Make calls
static void Button_Visual(Font_Obj font, int BGcolor, int BGcolor_hover, int FGcolor, int FGcolor_hover) {
  visual_inherit.font = font;
  visual_inherit.BGcolor_normal = BGcolor;
  visual_inherit.FGcolor_normal = FGcolor;
  visual_inherit.BGcolor_hover  = BGcolor_hover;
  visual_inherit.FGcolor_hover  = FGcolor_hover;
}

static void Button_Action(Button_Obj *btn) {
  btn->BGcolor = btn->BGcolor_normal;
  btn->FGcolor = btn->FGcolor_normal;
  for(int p=0; p<Controller.count; p++) {
    if(Controller.array[p]->x > btn->x &&
       Controller.array[p]->y > btn->y &&
       Controller.array[p]->x < btn->x+btn->w &&
       Controller.array[p]->y < btn->y+btn->h)
    {
      btn->BGcolor = btn->BGcolor_hover;
      btn->FGcolor = btn->FGcolor_hover;
      if(Controller.array[p]->wpaddown & WPAD_BUTTON_A) {
        MP3Player_PlayBuffer(Button_mp3, Button_mp3_size, NULL);
        btn->on_click(btn->click_arg);
      }
    }
  }
}

static void Button_Render(Button_Obj *btn) {
  GRRLIB_Rectangle( btn->x, btn->y, btn->w, btn->h, btn->BGcolor, 1);
  GRRLIB_Printf(
    btn->x+btn->w/2-( btn->font.w*strlen(btn->text) )/2,
    btn->y+btn->h/2-btn->font.h/2,
    btn->font.tex,
    btn->FGcolor,
    1,
    btn->text
  );
}
