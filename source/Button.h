#ifndef Button_File
#define Button_File

#include <grrlib.h>

#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <asndlib.h>
#include <mp3player.h>

#include "General.h"
#include "Textures.h"
#include "Controller.h"

//include generated audio header
#include "Button_mp3.h"

typedef struct {
  int self;
  int x;
  int y;
  int w;
  int h;
  int BGcolor;
  int BGcolor_normal;
  int BGcolor_hover;
  int FGcolor;
  int FGcolor_normal;
  int FGcolor_hover;
  Font_Obj font;
  char *text;
  void (*on_click)(int);
  int click_arg;
} Button_Obj;

Button_Obj Btn_Generic;

typedef struct {
  Button_Obj (*Make)(int x, int y, Button_Obj btn, void (*on_click)(int), int click_arg, char *text);
  void (*Visual)(Font_Obj font, int BGcolor, int BGcolor_hover, int FGcolor, int FGcolor_hover);
  void (*Action)(Button_Obj *btn);
  void (*Render)(Button_Obj *btn);
} Button_Class;
Button_Class Button;

void Button_Global_State_Change(int state);
void Button_Init();

#endif
