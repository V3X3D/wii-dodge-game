#include "Acid.h"

static void Acid_Create(int i);
static void Acid_Action(int i);
static void Acid_Delete(int i);
static void Acid_Render(int i);

static int i;

void Acid_Init() {
  Acid.Create = Acid_Create;
  Acid.Action = Acid_Action;
  Acid.Delete = Acid_Delete;
  Acid.Render = Acid_Render;

  for(i=0; i<MAX_ACID; i++)
    Acid.array[i] = NULL;
}

void Acid_Create_Rand_Timer(int chance, int range) {
  for(i=0; i<MAX_ACID; i++) {
    if(Acid.array[i] == NULL) {
      int r = rand() % range;

      if(r >= (range-chance))
        Acid.Create(i);

      return;
    }
  }
}

static void Acid_Create(int i) {
  int rspd = ( rand() % 4 )+1;
  int rsize = ( rand() % 28 )+16;
  int rcolor = ( rand() % 5 )+1;

  //Only can go half off on either side of the screen
  int rx = (rand() % SCREEN_WIDTH) - rsize/2;

  Acid.array[i] = malloc(sizeof(Acid_Obj));

  Acid.array[i]->self = i;      // Location in array
  Acid.array[i]->x    = rx;     // Random x of screen width
  Acid.array[i]->y    = -rsize; // y offset of size
  Acid.array[i]->w    = rsize;  // random w
  Acid.array[i]->h    = rsize;  // random h
  Acid.array[i]->spd  = rspd;   // random spd

  switch(rcolor) {
    case 1:
      Acid.array[i]->color = GRRLIB_PASTELRED;
      break;
    case 2:
      Acid.array[i]->color = GRRLIB_PASTELORANGE;
      break;
    case 3:
      Acid.array[i]->color = GRRLIB_PASTELYELLOW;
      break;
    case 4:
      Acid.array[i]->color = GRRLIB_PASTELGREEN;
      break;
    default:
      Acid.array[i]->color = GRRLIB_PASTELBLUE;
      break;
  }

  Acid.count++;
}

static void Acid_Action(int i) {
  Acid.array[i]->y += Acid.array[i]->spd;

  if(Acid.array[i]->y > SCREEN_HEIGHT && Acid.array[i] != NULL) {
    if(globalScore+5 < UINT_MAX) globalScore += 5;
    Acid_Delete(i);
  }
}

static void Acid_Delete(int i) {
  if(Acid.array[i] != NULL) {
    free(Acid.array[i]);
    Acid.array[i] = NULL;
    Acid.count--;
  }
}

static void Acid_Render(int i) {
  if(Acid.array[i])
    GRRLIB_Rectangle(
      Acid.array[i]->x,
      Acid.array[i]->y,
      Acid.array[i]->w,
      Acid.array[i]->h,
      Acid.array[i]->color,
      1
    );
}
