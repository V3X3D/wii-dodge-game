#ifndef Acid_File
#define Acid_File

#include <grrlib.h>
#include <stdlib.h>
#include <string.h>

#include <limits.h>

#include "General.h"

typedef struct {
  int self;
  int x;
  int y;
  int w;
  int h;
  int spd;
  int color;
} Acid_Obj;

typedef struct {
  void (*Create)(int i);
  void (*Action)(int i);
  void (*Delete)(int i);
  void (*Render)(int i);
  Acid_Obj *array[MAX_ACID];
  int count;
} Acid_Class;
Acid_Class Acid;

void Acid_Init();

void Acid_Create_Rand_Timer(int chance, int range);

#endif
