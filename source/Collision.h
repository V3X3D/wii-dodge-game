#ifndef Collision_File
#define Collision_File

#include <stdlib.h>
#include <stdbool.h>

bool Rect_Rect_Collision(int x1, int y1, int w1, int h1, int x2, int y2, int w2, int h2);

#endif
