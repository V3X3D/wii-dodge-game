#ifndef STATE_FILE
#define STATE_FILE

#include <grrlib.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <wiiuse/wpad.h>

#include "Textures.h"
#include "General.h"
#include "Controller.h"
#include "Player.h"
#include "Acid.h"
#include "Button.h"

#include "game-states/Home.h"
#include "game-states/Join.h"
#include "game-states/Play.h"

typedef struct {
  void (*Intro)();
  void (*Home)();
  void (*Join)();
  void (*Play)();
  void (*Exit)();
  void (*Quit)();
} State_Class;
State_Class State;

void State_Init();

#endif
