#include "Controller.h"

static void Controller_Create(int i);
static void Controller_Action(int i);
static void Controller_Delete(int i);
static void Controller_Render(int i);

static int i;

void Controller_Init() {
  Controller.Create = Controller_Create;
  Controller.Action = Controller_Action;
  Controller.Delete = Controller_Delete;
  Controller.Render = Controller_Render;

  for(i=0; i<MAX_CONTROLLER; i++)
    Controller.array[i] = NULL;
}

static void Controller_Create(int i) {
  u8 size = 16;

  WPAD_Init();
  WPAD_SetDataFormat(i, WPAD_FMT_BTNS_ACC_IR); // Will need to set to CHAN of controller

  WPAD_SetVRes(i, SCREEN_WIDTH, SCREEN_HEIGHT);

  Controller.array[i] = malloc(sizeof(Controller_Obj));

  Controller.array[i]->self = i; // Location in array
  Controller.array[i]->x    = -32; // Offscreen
  Controller.array[i]->y    = -32; // Offscreen
  Controller.array[i]->w    = size;
  Controller.array[i]->h    = size;

  switch(i) {
    case 1:
      Controller.array[i]->color = GRRLIB_GREEN;
      break;
    case 2:
      Controller.array[i]->color = GRRLIB_BLUE;
      break;
    case 3:
      Controller.array[i]->color = GRRLIB_YELLOW;
      break;
    default:
      Controller.array[i]->color = GRRLIB_RED;
  }


  Controller.count++;
}

static void Controller_Action(int i) {
  WPAD_IR(i, &Controller.array[i]->ir);

  Controller.array[i]->wpaddown = WPAD_ButtonsDown(i);
  Controller.array[i]->wpadheld = WPAD_ButtonsHeld(i);
  Controller.array[i]->wpadup   = WPAD_ButtonsUp(i);

  if(Controller.array[i]->ir.valid) {
    Controller.array[i]->x = Controller.array[i]->ir.x;
    Controller.array[i]->y = Controller.array[i]->ir.y;
  }

}

static void Controller_Delete(int i) {
  if(Controller.array[i] != NULL) {
    free(Controller.array[i]);
    Controller.array[i] = NULL;
    Controller.count--;
  }
}

static void Controller_Render(int i) {
  if(Controller.array[i])
    GRRLIB_Rectangle(
      Controller.array[i]->x-Controller.array[i]->w/2,
      Controller.array[i]->y-Controller.array[i]->h/2,
      Controller.array[i]->w,
      Controller.array[i]->h,
      Controller.array[i]->color,
      1
    );
}
