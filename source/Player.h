#ifndef Player_File
#define Player_File

#include <grrlib.h>

#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <asndlib.h>
#include <mp3player.h>

#include "General.h"
#include "Collision.h"
#include "Controller.h"
#include "Acid.h"

//include generated audio header
#include "Hit_mp3.h"

typedef struct {
  int self;
  int x;
  int y;
  int w;
  int h;
  int score;
  int color;
  bool dead;
  bool colliding;
} Player_Obj;

typedef struct {
  void (*Create)(int i);
  void (*Action)(int i);
  void (*Delete)(int i);
  void (*Render)(int i);
  Player_Obj *array[MAX_PLAYER];
  int count;
} Player_Class;
Player_Class Player;

void Player_Init();

#endif
