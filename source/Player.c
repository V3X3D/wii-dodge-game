#include "Player.h"

static void Player_Create(int i);
static void Player_Action(int i);
static void Player_Delete(int i);
static void Player_Render(int i);

static int i;

void Player_Init() {
  Player.Create = Player_Create;
  Player.Action = Player_Action;
  Player.Delete = Player_Delete;
  Player.Render = Player_Render;

  for(i=0; i<MAX_PLAYER; i++)
    Player.array[i] = NULL;
}

static void Player_Create(int i) {
  u8 size = 32;

  Player.array[i] = malloc(sizeof(Player_Obj));

  Player.array[i]->self = i; // Location in array
  Player.array[i]->x    = SCREEN_WIDTH/2-size/2;
  Player.array[i]->y    = SCREEN_HEIGHT/2-size/2;
  Player.array[i]->w    = size;
  Player.array[i]->h    = size;

  Player.array[i]->score     = 0;

  Player.array[i]->dead      = false;
  Player.array[i]->colliding = false;

  switch(i) {
    case 1:
      Player.array[i]->color = GRRLIB_GREEN;
      break;
    case 2:
      Player.array[i]->color = GRRLIB_BLUE;
      break;
    case 3:
      Player.array[i]->color = GRRLIB_YELLOW;
      break;
    default:
      Player.array[i]->color = GRRLIB_RED;
  }

  Player.count++;
}

static void Player_Action(int i) {

  //Collision with Acid
  for(int j=0; j<MAX_ACID; j++) {
    if(Acid.array[j] != NULL) {
      Player.array[i]->colliding = Rect_Rect_Collision
      (
        Player.array[i]->x, Player.array[i]->y, Player.array[i]->w, Player.array[i]->h,
        Acid.array[j]->x,   Acid.array[j]->y,  Acid.array[j]->w, Acid.array[j]->h
      );
    }
    if(Player.array[i]->colliding) {
      if(Player.array[i]->dead == false) MP3Player_PlayBuffer(Hit_mp3, Hit_mp3_size, NULL);
      Player.array[i]->dead = true;
    }
  }

  //Update cords
  Player.array[i]->x = Controller.array[i]->x;
  Player.array[i]->y = Controller.array[i]->y;

  //Keep player in bounds
  if(Player.array[i]->x < 0)
    Player.array[i]->x = 0;
  if(Player.array[i]->y < 0)
    Player.array[i]->y = 0;
  if(Player.array[i]->x+Player.array[i]->w > SCREEN_WIDTH)
    Player.array[i]->x = SCREEN_WIDTH-Player.array[i]->w;
  if(Player.array[i]->y+Player.array[i]->h > SCREEN_HEIGHT)
    Player.array[i]->y = SCREEN_HEIGHT-Player.array[i]->h;

  //Input
  if(Controller.array[i]->wpaddown & WPAD_BUTTON_HOME) {
    if(globalState == PLAY)      globalState = HOME;
    else if(globalState == HOME) globalState = PLAY;
  }

  if(!Player.array[i]->dead)
    Player.array[i]->score = globalScore;
}

static void Player_Delete(int i) {
  if(Player.array[i] != NULL) {
    free(Player.array[i]);
    Player.array[i] = NULL;
    Player.count--;
  }
}

static void Player_Render(int i) {
  if(Player.array[i] && !Player.array[i]->dead) {
    GRRLIB_Rectangle(
      Player.array[i]->x,
      Player.array[i]->y,
      Player.array[i]->w,
      Player.array[i]->h,
      Player.array[i]->color,
      1
    );
    GRRLIB_Rectangle(
      Player.array[i]->x+2,
      Player.array[i]->y+2,
      Player.array[i]->w-4,
      Player.array[i]->h-4,
      0xFFFFFFBB,
      1
    );
  }
}
